# CERN 中文欢迎手册计划 \| Welcome to CERN in Chinese Project

在CERN生活着许多来此工作学习的中文使用者，陌生的语言环境，迥异的风土人情，为初到CERN的人适应本地工作生活难免造成一定程度的阻碍。

本项目希望通过CERN GitLab搭建一个协作平台，由广大CERN中文用户共同参与，创建一个涵盖CERN、日内瓦州以及周边城市消息的欢迎手册。帮助初到CERN的人适应环境，也可以为所有中文使用者提供本地生活信息。

由于政策随时可能更新，关于各种行政手续的办理，请以CERN 或政府部门的最新通告为准，本手册仅供参考。

---

This Git-based project aims to create a guidebook for every Chinese-speaker working at CERN.

---

![](https://licensebuttons.net/l/by-nc/4.0/88x31.png)

本作品采用[知识共享署名-非商业性使用 4.0 国际许可协议](http://creativecommons.org/licenses/by-nc/4.0/)进行许可。

This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

