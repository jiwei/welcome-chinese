# 贡献列表

本项目由[Jiahui Wei](https://phonebook.cern.ch/phonebook/#personDetails/?id=824440) 于2017年11月30日创立，并负责最初的编辑及管理工作。

本项目从[Baosong Shan](https://phonebook.cern.ch/phonebook/#personDetails/?id=714435) 多年前创建的[TWiki 站点](https://twiki.cern.ch/twiki/bin/view/Sandbox/WelcomeToAMS)中获取了许多灵感，在此表示感谢。

## 参与人员名单（以最初参与时间为序）

### 2017年



