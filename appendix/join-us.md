# 加入我们

本手册是由个人发起的，基于Git 编写的开源电子书。源代码托管在CERN GitLab上。本站点由GitBook 生成，储存在CERN EOS空间上。

## 如何加入编写

任何拥有CERN 账号的用户均可以加入编写团队。对于编写者没有技术门槛限制，当然了解Markdown 语法和Git 的工作流程更佳。

### 我熟悉Git 的使用方法：

欢迎直接访问GitLab 了解本项目：[https://gitlab.cern.ch/jiwei/welcome-chinese](https://gitlab.cern.ch/jiwei/welcome-chinese)

### 我不懂Git 但希望参加编辑：

十分欢迎！请直接将您的草稿（最好遵循markdown 语法，当然普通文本也可以）发送邮件到[cernbox-project-cern-in-chinese-admins@cern.ch](mailto:cernbox-project-cern-in-chinese-admins@cern.ch)，管理员会定期将您的贡献整理并加入本项目。

## 如何加入管理维护团队

非常欢迎！如果您希望加入管理维护团队，请致信[cernbox-project-cern-in-chinese-admins@cern.ch](mailto:cernbox-project-cern-in-chinese-admins@cern.ch).

