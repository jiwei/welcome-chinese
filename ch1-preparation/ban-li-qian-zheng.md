# 办理签证

CERN属于设于瑞士的国际组织，因此签证办理上可以享受一定程度的便利，程序也与其它瑞士学生（工作）签证有所不同。

> **info-icon**
> **部分内容可能未能及时更新，请随时关注[CERN Users' Office](http://usersoffice.web.cern.ch/visa-requirement-and-procedure)
 关于签证政策的最新政策**



## 短期签证申请

## 长期签证申请（3个月以上）

如果需要在瑞士逗留90天以上，则必须向瑞士驻华外交机构申请长期签证（D 类）。

* 申请签证前，请先向有关实验组的秘书索要邀请信。来自CERN的邀请信是申请瑞士签证的重要凭据。
* 根据自己的所在领区，向大使馆或领事馆预约。对于长期D 类签证，瑞士要求必须本人亲自递交材料。
* 在预约时间提交材料后，使馆（领事馆）工作人员会告知领取签证时间，通常只需要3个工作日左右。
* 按照告知时间领取D 类签证。

**请注意**，无论申请时间长短，瑞士仅签发有效期为3个月的签证，请在到达瑞士后，尽快办理居留证件。获取居留证件后，居留证件起到了签证续签的作用，以后出入境凭借居留证件即可，无须再使用瑞士签证。

## 如需携家属

如果你计划带家人（配偶、子女）一起来，在请求邀请函时就可以请各个实验组的秘书把家人的信息一并写在邀请函和照会中，一并办理签证以及来CERN之后的注册和居住卡等。此时，可能需要提供相关材料证明家属关系。

---

## 相关链接

**更多有关签证的信息，可访问以下网站：**

* 瑞士驻华外交机构（大使馆及各领事馆）：[https://www.eda.admin.ch/countries/china/zh/home/representations.html](https://www.eda.admin.ch/countries/china/zh/home/representations.html)
* CERN Users' Office: [http://usersoffice.web.cern.ch/visa-requirement-and-procedure](http://usersoffice.web.cern.ch/visa-requirement-and-procedure)
* CERN 各实验组秘书: [http://usersoffice.web.cern.ch/experiment-secretariats](http://usersoffice.web.cern.ch/experiment-secretariats)

* CERN ATLAS Secretariat Visa Requirements: [https://espace.cern.ch/Invitationletters/ATLAS/SitePages/Home.aspx](https://espace.cern.ch/Invitationletters/ATLAS/SitePages/Home.aspx)



