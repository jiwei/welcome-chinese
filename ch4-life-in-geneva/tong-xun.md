# 通讯

CERN 是万维网（WWW）的发源地，CERN 所在的瑞士也在通信建设上长期处于世界领先水平。目前瑞士是世界上通信网络最发达的国家之一。

## 手机

### 运营商的选择
4G 网络目前已经广泛覆盖瑞士全境，主要的移动运营商有三家：

* Swisscom：曾经的国营公司，目前依然有国家控股，在瑞士用户人数最多，理论上拥有最佳的信号覆盖。
* Sunrise： 新兴的运营商之一，价格相对Swisscom 更低，信号覆盖同样不错。
* Salt： 曾经叫Orange，跨国通讯集团Orange SA的一部分，目前更名为Salt，为三家运营商里最年轻的一家，价格低，城市地区信号尚佳，然而在郊区、地下等地区信号相比其余两家较差。
* \*CERN Mobile Services: CERN也提供可供工作使用的手机卡服务，可以CERN EDH网站上申请，详情见[http://information-technology.web.cern.ch/services/fe/info/mobile-services-overview](http://information-technology.web.cern.ch/services/fe/info/mobile-services-overview)

除了以上三家自己搭建移动蜂窝网络的运营商外，瑞士还存在许多其它的通讯公司（虚拟运营商），他们的服务运营在上述三家的网络之上，但是通常能提供更低的价格，不过也要注意看他们的服务条款。常见的公司有：

* M-budget Mobile： 便宜，合作商是Swisscom。
* Coop Mobile
* Aldi Mobile
* Yallo： 如果想寻找相对廉价的无限流量套餐是个不错的选择。
* Lycamobile： 向海外拨打电话较多的话可以考虑。
* ……

若为临时使用，在CERN Meyrin园区R1食堂边的小卖部里便可以买到价格相对便宜的手机卡了。

由于CERN Meyrin园区在瑞士，而如果您住在法国地区，在选择手机套餐时请确认能够覆盖两国。

> **info-icon**
> 经人肉测试，Salt 的网络在边境地区较差，Prevessin 站区完全无信号；
> Swisscom 和Sunrise 在法国境内有微弱信号。如果经常需要在瑞士境外
> 工作，可能需要考虑双卡或者办理漫游套餐。

### 运营商更换

如果在各个运营商之间难以抉择，不要担心，目前运营商之间可以直接携号转网，只要不是在合同期内（比如同时办理了手机合约这种情况），就可在保留当前号码的同时更换运营商。

## 宽带

目前光纤网络已经广泛普及，多数运营商可以提供高达1 Gbps 的带宽。

