# 房源信息

## CERN 提供的租房服务

CERN 提供如下几种住房服务：

* CERN Hotel：CERN 自营的宾馆，最长居住时间三个月。
* CERN Apartment：CERN 运营的公寓，需要提前申请。
* CERN Private Market：私人广告发布，房东在网页上发布房源信息，CERN 对信息真实性不负责。
* CERN Market：利用CERN Social搭建的二手交易平台，经常可以在上面找到房屋转租等信息。

Facebook上 Young @ CERN: [https://www.facebook.com/groups/young.at.cern/ ](https://www.facebook.com/groups/young.at.cern/)一个非常有用的交流群，有一些房源。

## 日内瓦大学城

Cité Universitaire de Genève 是日内瓦最大的学生宿舍，除服务于日内瓦大学的以外，也面向广大有长期或短期居住需求的留学生开放申请。提供多种房型，从多人间到studio 都有提供，但是需要提早申请。价格相对低廉，劣势是没有直达CERN 的公交，通勤时间较长。

## 非营利性组织

The Geneva Welcome Centre's \(CAGI\) 是一个为在日内瓦国际组织工作的人提供安居、活动信息等各项服务，帮助其融入本地生活的非营利性组织，可以尝试在CAGI 网站上注册信息，CAGI 会将房源信息以邮件形式推送。

## 商业租房网站

瑞士的房产公司通常会将房源信息发布到自建的网站或者home.ch 这种不动产信息平台。一些本地网站（法语）上也可以找到许多个人房东的转租信息。

[Comparis.ch](https://en.comparis.ch) 是一个提供本地比价服务的网站，其中汇总了几大租房平台以及多家中介公司的信息，还可以查询某一地区历史成交价格，可以用于参考。


