# Payment Slip

Payment Slip 是瑞士常见的账单支付方式，payment slip 有两种：
- 红色的ES（德语：Einzahlungsschein），英语通常叫red slip，没有reference number，可以全手写，可以在右侧填写附加信息；
![](https://upload.wikimedia.org/wikipedia/commons/7/7e/Einzahlungsschein.jpg)
- 橘色的ESR（德语：Einzahlungsschein mit Referenznummer），英语通常称为orange slip，主要特点是增加了一串reference number。对于需要处理大量业务的收款方来说，可以很容易的将这串27位的数字与账单绑定，便于管理每一笔入账。下图为尚未打印上账户信息的ESR；
![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Esr_schweiz.jpg/640px-Esr_schweiz.jpg)

如果你的手机等服务选择了纸质账单，那么这些公司会将账单并slip 一同邮寄。你同样可以通过给别人印有自己信息的payment slip 的方式收款，有现金转账需要时还可以在邮局手写slip 进行转账。

如果自己需要通过slip 收款，可以直接向自己银行账户的开户行订购。

已经写好的slip，可以通过多种方式支付：
- 直接在邮局柜台用现金或PostFinance 卡支付（不接受一般银行卡）；
- 通过银行的自助服务机扫描处理，这些机器通常在ATM 附近；
- 手机银行扫描账单支付。

不过，现在大多数公司都提供了电子账单等更便利节约的支付方式，很多时候还能省去每月的账单费，不失为更好的选择。