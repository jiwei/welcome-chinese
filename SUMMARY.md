# Summary

* [简介](README.md)

## 基本信息

* [了解CERN](ch0-info/CERN.md)
* [了解瑞士](ch0-info/Switzerland.md)
* [了解日内瓦](ch0-info/Geneva.md)

## 行前准备

* [办理签证](ch1-preparation/ban-li-qian-zheng.md)
* [购买机票](ch1-preparation/gou-mai-ji-piao.md)
* [行李准备](ch1-preparation/xing-li-zhun-bei.md)

## 抵达CERN

* [抵达CERN](ch2-on-your-arrival/di-da-cern.md)
* [选择住处](ch2-on-your-arrival/an-pai-zhu-fang.md)
* [办理居留证件](ch2-on-your-arrival/ban-li-ju-zhu-zheng-jian.md)
* [办理银行账户](ch2-on-your-arrival/ban-li-yin-hang-zhang-hu.md)
* [办理医疗保险](ch2-on-your-arrival/ban-li-yi-liao-bao-xian.md)

## 生活在法国

## 生活在日内瓦

* [租房](ch4-life-in-geneva/zu-fang.md)
  * [房源信息](ch4-life-in-geneva/zu-fang/fang-yuan-xin-xi.md)
  * [租房材料](ch4-life-in-geneva/zu-fang/zu-fang-cai-liao.md)
  * [入住之后](ch4-life-in-geneva/zu-fang/ru-zhu-zhi-hou.md)
  * [垃圾回收](ch4-life-in-geneva/zu-fang/la-ji-hui-shou.md)
* [购物](ch4-life-in-geneva/gou-wu.md)
  * [超市](ch4-life-in-geneva/gou-wu/chao-shi.md)
  * [商圈和购物中心](ch4-life-in-geneva/gou-wu/gou-wu-zhong-xin.md)
  * [网上购物](ch4-life-in-geneva/gou-wu/wang-shang-gou-wu.md)
* [通讯](ch4-life-in-geneva/tong-xun.md)
* [交通](ch4-life-in-geneva/jiao-tong.md)
  * [市内公交](ch4-life-in-geneva/jiao-tong/shi-nei-jiao-tong.md)
  * [瑞士铁路](ch4-life-in-geneva/jiao-tong/rui-shi-tie-lu.md)
  * [驾车出行](ch4-life-in-geneva/jiao-tong/jia-che-chu-xing.md)
* [金融](ch4-life-in-geneva/jin-rong.md)
  * [瑞士法郎](ch4-life-in-geneva/jin-rong/rui-shi-fa-lang.md)
  * [银行账户](ch4-life-in-geneva/jin-rong/yin-xing-zhang-hu.md)
  * [信用卡](ch4-life-in-geneva/jin-rong/xin-yong-qia.md)
  * [Payment Slip](ch4-life-in-geneva/jin-rong/payment-slip.md)
* [邮政](ch4-life-in-geneva/you-zheng.md)

## 关于本手册

* [加入我们](appendix/join-us.md)
* [贡献列表](appendix/author-list.md)

