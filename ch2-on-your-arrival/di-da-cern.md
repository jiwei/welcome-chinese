# 抵达CERN

## 到达日内瓦机场

经过漫长的旅程，飞机终于抵达瑞士，降落前，可以在机舱里看到日内瓦湖的美丽景色。

抵达日内瓦机场后，在行李等候区靠近出口的位置，有一台机器，只要按下按钮即可获得一张日内瓦公交票，凭借这张公交票，可以在**一小时内**任意乘坐日内瓦所有市内公共交通（公交车、有轨电车、市内火车、轮渡等）。

## 从机场到市区

如果选择了日内瓦市区的酒店，可以直接凭公交票在机场火车站乘坐火车到日内瓦Cornavin 火车站，不需要检票，不限定座位（仅限二等座），直接上车即可。

亦可以在Blandonnet 站乘坐Tram 18 到达市区。

## 从机场到 CERN

从机场到CERN Meyrin园区可以选择Y bus直达，或者到Blandonnet站之后转18路Tram。公交路线查询：[http://tpg.ch/en/web/site-international。](http://tpg.ch/en/web/site-international)

CERN 配有连接机场及Meyrin 园区的shuttle，如果住在CERN 或者希望直接去CERN 报道的人可以选择在机场等候此班车。班车为4号线，车身上有醒目的“CERN”标识，一般不会错过。班车的时间表，可以在网上查看：[https://smb-dep.web.cern.ch/en/ShuttleService/Circuit4 ](https://smb-dep.web.cern.ch/en/ShuttleService/Circuit4).

（班车这里是否需要查验邀请信？请乘坐过的人补充）

