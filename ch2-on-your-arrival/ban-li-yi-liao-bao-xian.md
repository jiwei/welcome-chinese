# 办理医疗保险

根据法律要求，所有在CERN 工作的人员都需要购买同时符合瑞士和法国要求的医疗保险。

> “Associated members of the personnel and their accompanying family members shall have health insurance cover that is adequate in both Host States (France and Switzerland) for the financial consequences of illness and accidents, including occupational illness and accidents for the associated members of the personnel. … The loss of insurance cover entails automatic termination of the contract of association.”

## 瑞士医保系统

对于CERN 的雇员来说，保险方面最严格的限制来自瑞士的强制医疗保险政策。依照瑞士法律，所有在瑞士居住及工作的本国及外国人，均必须购买符合瑞士规定的医疗保险。

瑞士虽然没有国营健康保险，但是作为替代，拥有一套复杂而价格昂贵的全国保险系统。如果你选择居住在日内瓦，在定居下来后，登记的住址处会很快收到来自州保险办公室（Service de l'assurance- maladie， SAM）的信函，告知你需要购买符合条件的保险，需要在三个月内购买保险或提交已经购买保险证明，否则会被强制指派一份价格不菲的保险。如果希望体验瑞士的医保系统，并且愿意付出一个月300 CHF起步的保费的话，可以考虑参照随信发放的清单里选择购买一份瑞士医保。

不过，对于在CERN 工作的学生及科研人员来讲，通常符合如下条件，从而可以通过购买境外保险获得豁免（dispense）：
- 居住在瑞士，身份为学生；
- 居住在瑞士，不是学生，但在国际组织工作（包括CERN）；
- 不居住在瑞士，但需要跨境在瑞士工作；

针对上述这些情况，有许多的替代方案可以帮助你避开昂贵的瑞士保险。

## 国内医疗保险

许多人选择使用国内的商业健康保险，例如利宝、太平洋、平安等境外医疗保险，其中利宝包括了牙医。

国内保险普遍比较便宜，当然对应的保额较低（多数3万欧到5万欧），保障也比较受限，多数仅包括急诊，既有健康问题往往也不在保障范围之内。另外很多国内的保险都对出国时间长短有限制，购买之前一定要确认清楚。

## CERN提供的健康保险

### CHIS by UNIQA

UNIQA是CERN的长期保险合作商，保障范围很广，门诊、急诊、住院、牙科、眼镜、疾病筛查等等，当然价格不菲，它的价格每年都在变，一般都在每月1000瑞郎以上，当然包括全家的保障（配偶和子女）。需要特别提醒的是，你只有在来CERN注册或者变更所属单位的一个月之内才可以订购UNIQA保险，过了这一个月，就不再接收你了：）

> **danger-icon**
> **2017年9月起，UNIQA已经停止接受新的人员**
>
> "From 1 September 2017: no new voluntary membership will be possible. [III 3.01]"

### Allianz Partners Healthcare Plan

也许因为UNIQA过于昂贵，费率也不灵活，CERN 同时提供了另外一个选择，即安联为CERN量身定做的保险，大约每月每人140欧元的保费，不论年龄，CERN的用户和家属（配偶和孩子）都可以购买，网上就可以办理。

这个保险的保障范围没有UNIQA那么全面，包括了门诊（90%）、住院（普通病房100%）、急诊、牙科急诊、眼镜破碎等，详情请参考具体条款。对比国内保险肯定会更贵，但是保额更高（250万欧）。


## 其它面向在瑞外国人的商业医保

这些医疗保险并非瑞士医保体系的一部分，也和CERN 没有合作关系。他们通常是由设立在欧洲其他国家的保险公司承包，针对在瑞士的外国人群体进行销售。这些保险通常提供100 CHF/月以下的廉价方案，在办理好这类医保后，保险公司会给州保险办公室致函证明你已经拥有了符合瑞士标准的保险，进而免于购买瑞士医保。

### SwissCare

做得比较早的一家保险，官网提供中文介绍页面，在留学生群体中知名度颇高，学生群体里购买人数不少，（服务待评价）

### Student Care

另外一家学生保险公司。

### April Euro Cover

这是一个欧洲范围有效的保险，也是目前不多接收居住在法国的CERN成员的健康主险之一。这个保险分为三个不同等级的方案，分别有不同的保额上限（75万欧到200万欧），以及不同的报销额度。另外，如果选择每个账单自付20欧或40欧，报价也会有所变化。作为参考，一个四口之家如果选择最低等级方案和每账单40欧自付额度，每月的保费大约为500欧（2017年12月份的报价）。

- 门店：Assurances Stéphane Pibouleau （AREAS）
- 地址：[2 Rue du Bordeau, 01630 Saint-Genis-Pouilly, France](https://www.google.fr/maps/place/Assurances+St%C3%A9phane+Pibouleau/@46.2429413,6.0209395,21z/data=!4m5!3m4!1s0x478c627e061ccb5b:0x381a3629085e837f!8m2!3d46.2429082!4d6.0207298)
- [在线报价](https://tarif-expat.april-international.com/aie-public/tarificateur.jsf?cid=1)

#### Scorestudies
April Euro Cover 针对学生定制的产品，日内瓦大学的合作保险之一，可直接通过网站购买，实际提供商是安联保险。



